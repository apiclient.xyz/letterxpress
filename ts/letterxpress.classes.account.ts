import { IObjectAction } from '@tsclass/tsclass/dist_ts/database/objectaction.js';

import * as plugins from './letterxpress.plugins.js';

export interface ILetterXpressConstructorOptions {
  username: string;
  apiKey: string;
}

export class LetterXpressAccount {
  // STATIC
  public static async createAndInit(optionsArg: ConstructorParameters<typeof LetterXpressAccount>[0]) {
    const letterXpressInstance = new LetterXpressAccount(optionsArg);
    await letterXpressInstance.init();
    return letterXpressInstance;
  }

  // INSTANCE
  public baseApiUrl = 'https://api.letterxpress.de/v1';
  public options: ILetterXpressConstructorOptions;
  public smartletterInstance: plugins.smartletter.Smartletter;

  public letterSentObservable = new plugins.smartrx.rxjs.Subject<plugins.tsclass.business.ILetter>();

  constructor(optionsArg: ILetterXpressConstructorOptions) {
    const done = plugins.smartpromise.defer();
    this.options = optionsArg;
  }

  public async init() {
    this.smartletterInstance = await plugins.smartletter.Smartletter.createAndInit();
  }

  /**
   * sends a letter
   * @param letterArg
   */
  public async sendLetter(letterArg: plugins.tsclass.business.ILetter) {
    const letter = await this.smartletterInstance.createLetterFromData(letterArg);
    const pdfToSend = await letter.getCombinedPdf();
    const response = await this.request('/setJob', 'POST', {
      letter: {
        base64_file: plugins.smartbuffer.arrayBufferToBase64(pdfToSend.buffer),
        base64_checksum: await plugins.smarthash.md5FromString(
          plugins.smartbuffer.arrayBufferToBase64(pdfToSend.buffer)
        ),
        specification: {
          color: '4',
          mode: 'simplex',
          ship: letterArg.to.address.country === 'Germany' ? 'national' : 'international',
        },
      },
    });
    letterArg.objectActions.push({
      name: 'letterxpress-sent',
      message: 'requested letterxpress to send the letter',
      privateMessage: null,
      data: response.body.letter.job_id,
      timestamp: Date.now(),
      userId: null
    })
    return letterArg;
  }

  public async cancelLetter(letterArg: plugins.tsclass.business.ILetter) {
    const processingId = letterArg.objectActions.reduce<string>((previousVal, currentVal) => {
      if (currentVal.name === 'letterxpress-sent') {
        return currentVal.data
      } else {
        return previousVal;
      }
    }, null);
    return await this.cancelLetterByProcessingId(processingId);
  }

  public async cancelLetterByProcessingId(processingId: string) {
    const response = await this.request(`/deleteJob/${processingId}`, 'DELETE', {
      letter: {}
    });
    return response;
  }

  /**
   * fires the request
   */
  private async request(routeArg: string, methodArg: 'GET' | 'POST' | 'DELETE', payload: any) {
    const requestUrl = `${this.baseApiUrl}${routeArg}`;
    console.log(requestUrl);
    const requestData = {
      auth: {
        username: this.options.username,
        apikey: this.options.apiKey,
      },
      ...payload,
    };
    const response = await plugins.smartrequest.request(requestUrl, {
      method: methodArg,
      headers: {
        'Content-Type': 'application/json',
      },
      requestBody: JSON.stringify(requestData),
    });
    console.log(response.body);
    return response;
  }

  public async stop() {
    await this.smartletterInstance.stop();
  }
}
