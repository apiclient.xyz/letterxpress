import { expect, tap } from '@pushrocks/tapbundle';
import * as letterxpress from '../ts/index.js';
import * as tsclass from '@tsclass/tsclass';

import { Qenv } from '@pushrocks/qenv';
let testQenv = new Qenv('./', './.nogit/');

let testAccount: letterxpress.LetterXpressAccount;
let testLetter: tsclass.business.ILetter;

tap.test('should create a valid account', async () => {
  testAccount = await letterxpress.LetterXpressAccount.createAndInit({
    apiKey: testQenv.getEnvVarOnDemand('API_TOKEN'),
    username: testQenv.getEnvVarOnDemand('API_USERNAME'),
  });
  expect(testAccount).toBeInstanceOf(letterxpress.LetterXpressAccount);
});

tap.test('should send an actual letter', async () => {
  testLetter = {
    from: {
      name: 'Lossless GmbH',
      type: 'company',
      description: 'a company',
      sepaConnection: {
        iban: 'NL83 BUNQ 2035 5639 41',
        bic: 'BUNQNL2AXXX',
      },
      address: {
        name: 'default',
        city: 'Bremen',
        country: 'Germany',
        postalCode: '28359',
        houseNumber: '5',
        streetName: 'Karl-Ferdinand-Braun-Str.',
      },
    },
    to: {
      name: 'Lossless GmbH',
      type: 'company',
      description: 'a company',
      address: {
        city: 'Bremen',
        country: 'Germany',
        postalCode: '28359',
        streetName: 'Karl-Ferdinand-Braun-Str.',
        houseNumber: '5',
      },
    },
    incidenceId: 'abc123',
    legalContact: {
      type: 'company',
      salutation: null,
      surname: null,
      title: null,
      address: {
        city: 'Bremen',
        country: 'Germany',
        name: 'Lossless GmbH',
        postalCode: '28359',
        streetName: 'Karl-Ferdinand-Braun-Str.',
        houseNumber: '5',
      },
      sepaConnection: {
        iban: 'NL83 BUNQ 2035 5639 41',
        bic: 'BUNQNL2AXXX',
      },
      customerNumber: null,
      description: null,
      email: 'hello@lossless.com',
      fax: '+49 421 408951 46',
      phone: '+49 421 16767 548',
      legalEntity: 'Lossless GmbH',
      name: 'Lossless GmbH',
      vatId: 'DE293580420',
    },
    subject: 'General Terms - Update',
    text: [
      `
To whome it may concern, 
           
**this is a testmessage.** we write to inform you about a change in our General Terms.
As of December 1st 2019 we will start storing IPs that connect to our app for a period of 3 month.

Regards  
Lossless GmbH - Legal Department

`,
    ],
    date: Date.now(),
    language: 'DE',
    logoUrl: '',
    needsCoverSheet: true,
    objectActions: [],
    pdfAttachments: [],
    timesheetData: null,
  };
  await testAccount.sendLetter(testLetter);
});

tap.test('should be able to delete the sending job', async (toolsArg) => {
  await toolsArg.delayFor(3000);
  await testAccount.cancelLetter(testLetter);
});

tap.test('should wrap things up', async () => {
  await testAccount.stop();
  console.log(testLetter);
});

tap.start();
